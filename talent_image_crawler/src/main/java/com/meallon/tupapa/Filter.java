package com.meallon.tupapa;

public interface Filter {  
	  
    public boolean accept(String url);  
}  