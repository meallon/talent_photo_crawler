package com.meallon.tupapa;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.LinkedBlockingDeque;

public class MyLinkBlockQueue {  
	  
    // 已访问的 url 集合  
    private  CopyOnWriteArraySet <String> visitedUrls = new CopyOnWriteArraySet <String>(); 
  
    // 未访问的url  
    private  LinkedBlockingDeque<String> unVisitedUrls = new LinkedBlockingDeque<String>();  
      
    // 未访问的URL出队列  
    public   String getUnVisitedUrl() {  
        String url = null;
        try {
        	url = unVisitedUrls.take();
        	visitedUrls.add(url);
		} catch (InterruptedException e) {
		
			e.printStackTrace();
		}
        return url;
    }  
      
      
      
    // 新的url添加进来的时候进行验证，保证只是添加一次  
    public   void addUnvisitedUrl(String url) {  
        if (url != null && !url.trim().equals("") && !visitedUrls.contains(url)  
                && !unVisitedUrls.contains(url))
			try {
				unVisitedUrls.put(url);
			} catch (InterruptedException e) {
			
				e.printStackTrace();
			}  
    }  
      

      
}  