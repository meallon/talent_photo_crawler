package com.meallon.tupapa;
/**
 * 图片爬虫的例子
 * @author panminlan
 * @时间 20170104
 */
public class demo {

	public static void main(String[] args){
		Crawler crawler = new Crawler();
		crawler.setDirName("C:/picc/");//设置图片保存的目录，默认是C:/douban/
		crawler.setTHREAD_NUMS(3); //设置爬取网络地址的线程数,如果不设的话默认是cup核心数的两倍
		UrlDefaultFilter filter = new UrlDefaultFilter();//生成url过滤器，过滤掉不需要爬取的地址
		crawler.crawling("http://photo.sina.com.cn/",filter);
	}
}
