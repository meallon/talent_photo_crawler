package com.meallon.tupapa;

import java.util.HashSet;  
import java.util.Set;  
import org.htmlparser.Node;  
import org.htmlparser.NodeFilter;  
import org.htmlparser.Parser;  
import org.htmlparser.filters.NodeClassFilter;  
import org.htmlparser.filters.OrFilter;
import org.htmlparser.filters.TagNameFilter;
import org.htmlparser.tags.LinkTag;  
import org.htmlparser.util.NodeList;  
import org.htmlparser.util.ParserException;  
  
/** 
 * 获取一个页面所有的超链接
 * @author panminlan 
 * 
 */  
public class ParserHttpUrl {  
      
   
    public static Set<String> extracLinks(String url,Filter filter) {  
        Set<String> links = new HashSet<String>();  
        try {  
            Parser parser = new Parser(url);  
           
                           
            parser.setEncoding("UTF-8");
            TagNameFilter tagFilter=new TagNameFilter("A");
            
            NodeList list = parser.extractAllNodesThatMatch(tagFilter);  
         
            for (int i = 0; i < list.size(); i++) {  
                Node tag = list.elementAt(i);                  
                if (tag instanceof LinkTag)
                {  
                    LinkTag link = (LinkTag) tag;  
                    String linkUrl = link.getLink();
                    if (filter.accept(linkUrl))  
                        links.add(linkUrl);  
                }
            }  
        } catch (ParserException e) {  
            e.printStackTrace();  
        }  
        return links;  
    }  
}  