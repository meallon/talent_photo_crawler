package com.meallon.tupapa;

public class CountDowns {

	public  int counts = 0;

	public int getCounts() {
		return counts;
	}

	public void setCounts(int counts) {
		this.counts = counts;
	}
	
	public CountDowns(int setCounts){
		this.counts = setCounts;
	}
	
	synchronized void decreaseCounts(){
		this.counts--;
	}
	
}
